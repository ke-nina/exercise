package com.kenitu.homework;

import java.util.Objects;

public class Sorter {

	public void traversal(int[] array) {
		if (Objects.isNull(array)) {
			System.out.println("数组为空，不能比较");
			return;
		}
		if (array.length == 0) {
			System.out.println("该数组长度为零，不能比较");
			return;
		}
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + (i == array.length - 1 ? "\t" : ","));
		}
	}

	public void sort(int[] array) {
		int temp = 0;
		for (int i = 0; i < array.length - i; i++) {
			for (int j = 0; j < array.length - i - 1; j++) {
				if (array[j] > array[j + 1]) {
					temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
	}

	public static void main(String[] args) {
		Sorter sorter = new Sorter();
		int[] a = { 1, 100, -20, 99, 1000, 0, 30 };
		System.out.print("排序前 : ");
		sorter.traversal(a); // 排序前 : 1 , 100 , -20 , 99 , 1000 , 0 , 30
		sorter.sort(a);
		System.out.print("排序后 : ");
		sorter.traversal(a); // 排序后 : -20 , 0 , 1 , 30 , 99 , 100 , 1000
	}
}
