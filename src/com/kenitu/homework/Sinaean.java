package com.kenitu.homework;

public class Sinaean {
       String name;
       char gender;
       int age;
       boolean married;
       public void marry(Sinaean another) {
    	   if(this.married==true&&another.married==false) {
    		   System.out.println("双方不能结婚，因为一方已婚");
    		   return;
    	   }
    	   if(this.age<22||another.age<20) {
    		   System.out.println("双方不能结婚，因为不满足法定年龄");
    		   return;
    	   }
    	   if(this.gender==another.gender) {
    		   System.out.println("双方不能结婚，性别相同不能结婚"); 
    		   return;
    	   }
    		   System.out.println("符合规定可以结婚");
      
       }
       public static void main(String[] args) {
		Sinaean t=new Sinaean();
		 t.name="小黄";
		 t.gender='女';
		 t.age=20;
		 t.married=false;
		Sinaean a=new Sinaean();
		 a.name="小绿";
		 a.gender='男';
		 a.age=22;
		 a.married=false;
		 a.marry(t);
		
	}
}
