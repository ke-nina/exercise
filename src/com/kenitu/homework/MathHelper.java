package com.kenitu.homework;

public class MathHelper {
	
		public static MathHelper getInstance(){
			MathHelper m=new MathHelper();
			return m;
	}
		public long max( long first , long... values ) {
			long max=values[0];
			for (int i = 1; i < values.length; i++) {
				if(values[i]>max){
				max=values[i];
				}
			}
			return max;
			
		    // 找出参数中的最大值并返回
		}
		public double max( double first , double... values ) {
			double max=values[0];
			for (int i = 1; i < values.length; i++) {
				if(values[i]>max){
				max=values[i];
				}
			}
			return max;
		    // 找出参数中的最大值并返回
		}
		public long min( long first , long... values ) {
			long min=values[0];
			for (int i =1; i < values.length; i++) {
				if(values[i]<min){
				min=values[i];
				}
			}
			return min;
		    // 找出参数中的最小值并返回
		}
		public double min( double first , double... values ) {
			double min=values[0];
			for (int i = 1; i < values.length; i++) {
				if(values[i]<min){
				min=values[i];
				}
			}
			return min;
		    // 找出参数中的最小值并返回
		}
		
		
}
