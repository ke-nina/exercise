package com.kenitu.homework;

public class AnimalTest {
	public static void main(String[] args) {
		Animal a = null ;
		a = new Phoenix( "鸟类-凤凰" );
		System.out.println( a.toString() );
		a.eat("小鱼干" );
		
		a = new Eagle( "鸟类-老鹰" );
		System.out.println( a.toString() );
		a.eat( "小兔子" );

		a = new Bear("兽类-熊" );
		System.out.println( a.toString() );
		a.eat( "鱼" );

		a = new Lion( "兽类-狮子" );
		System.out.println( a.toString() );
		a.eat( "羊");
	}
}
