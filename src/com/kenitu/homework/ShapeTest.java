package com.kenitu.homework;

public class ShapeTest {
	public static void main(String[] args) {
		Triangle1 t = new Triangle1(); // 创建 Triangle 实例
		t.firstEdge = 44 ; // 设置边长
		t.secondEdge = 49 ; // 设置边长
		t.thirdEdge = 52 ; // 设置边长
		t.calculate() ; // 计算三角形面积
		t.description(); // 输出三角形信息和三角形面积
		
		Circle c = new Circle(0);
		c.radius = 8 ; // 设置半径
		c.calculate() ; // 计算圆面积
		c.description(); // 输出圆半径和圆面积
}
}
