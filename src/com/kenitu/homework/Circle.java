package com.kenitu.homework;

public class Circle extends Shape {
         public double radius;

         
         
		public Circle(double radius) {
			super();
			this.radius = radius;
		}
         
         public void calculate() {
        	 double r=radius*radius;
        	 area=Math.PI*r;
         }
         
         public void description() {
 			this.type="圆形";
 			System.out.println("圆的半径为"+this.radius);
 		
 			super.show();
         
   }
}