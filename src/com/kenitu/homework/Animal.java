package com.kenitu.homework;

public class Animal {
	  private final String type ; // 某个个体对应的动物种类
	    protected String name ; // 某个个体的名称
	    protected int age ; // 某个个体的年龄
	    
	    
	    public Animal( String type ) {
	        this.type = type ;
	    }
	    
	    public Animal( String type , int age ) {
	        this.type = type ;
	        this.age = age ;
	    }
	    
	    public void eat( String food ) {
	        System.out.println( "吃" + food );
	    }
	    
	    @Override
	    public String toString(){
			return "动物类别:"+type+",年龄:"+age;
	        // 返回 Animal 对象的字符串形式
	    }
      
}
