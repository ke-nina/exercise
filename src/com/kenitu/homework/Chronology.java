package com.kenitu.homework;

import java.util.Scanner;

public class Chronology {
	// private 表示私有的
	private static final char[] HEAVENLY_STEMS = { '甲' , '乙' , '丙' , '丁' , '戊' , '己' , '庚' , '辛' , '壬' , '癸' }; // 天干
	// static 表示与类相关的、属于类的
	private static final char[] EARTHLY_BRANCHES={ '子' , '丑' , '寅' , '卯' , '辰' , '巳' ,  '午' , '未' , '申' , '酉' , '戌' , '亥' }; // 地支
	// final 表示最终的、不可更改的
	private static final char[] CHINESE_ZODIAC = { '鼠','牛','虎','兔','龙','蛇','马','羊','猴','鸡','狗','猪' } ;  // 生肖
	
	private static final int CONTRAST = 1984 ;// 参照年份
	
	
	public static void show(){
         for( int i = 1 ; i <= 60 ; i++ ) {
			
			int j = i - 1 ;
			int stemsIndex = j % HEAVENLY_STEMS.length ;
			char steam =HEAVENLY_STEMS[ stemsIndex ];
			
			int branchesIndex = j % EARTHLY_BRANCHES.length ;
			char branche = EARTHLY_BRANCHES[ branchesIndex ];
			
			System.out.print( steam + "" + branche + ( i % 12 == 0 ? "\n" : "\t" ) );
			
	}
  }
	public static String seek( int year ) {
		String name = Chronology.seek( 1989);
		System.out.println( name ) ; // 输出 乙丑
		return name;
	}
	public static char sign( int year ) {
		char s = Chronology.sign( 1996 );
		System.out.println( s ) ; // 输出 鼠
		return s;   
	}
	public static void main(String[] args) {
		Chronology c=new Chronology();
		c.show();
		c.sign(0);
		
	}
}