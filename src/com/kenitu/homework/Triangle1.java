package com.kenitu.homework;

public class Triangle1 extends Shape{
		protected double firstEdge ;
		protected double secondEdge ;
		protected double thirdEdge ;
	
		
		
		public Triangle1(){
			
	      
	}
		
		
		public void calculate() {
		    // 在这里计算三角形的面积，并将面积存储到 area 变量中 ( area 是从父类继承的、可见的实例变量 )
			double p=(firstEdge+secondEdge+thirdEdge)/2;
			area=Math.sqrt(p*(p-firstEdge)*(p-secondEdge)*(p-thirdEdge));
		
		}
		
		public void description() {
			this.type="三角形";
			System.out.println("三角形的第一条边长为"+this.firstEdge);
			System.out.println("三角形的第二条边长为"+this.secondEdge);
			System.out.println("三角形的第三条边长为"+this.thirdEdge);
			super.show();
		    // 在这里输出三角形基本信息(比如三边的长度)
		    // 最后通过调用从父类继承的、可见的 show 方法输出 三角形的面积
		}
		
}
