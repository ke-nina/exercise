package com.kenitu.homework;

public class Shape {
	protected double area ;
	protected String type ; 
	

	public void show(){
	    // 在 Shape 类中 show 方法除了输出语句外，没有任何其它代码
	    System.out.println( this.type + "的面积为" + this.area );
	}
}
