package com.kenitu.homework;

import java.util.Scanner;

public class ArrayHelper {

	public static boolean equal(int[] first, int[] second) {
		if (first.length != second.length) {
			return false;
		}
		for (int i = 0; i < second.length; i++) {
			if (first[i] != second[i]) {
				return false;
			}
		}

		return true;

	}

	public static boolean equal(char[] first, char[] second) {
		if (first.length != second.length) {
			return false;
		}
		for (int i = 0; i < second.length; i++) {
			if (first[i] != second[i]) {
				return false;
			}
		}

		return true;

	}

	

	public static void main(String[] args) {
		ArrayHelper a = new ArrayHelper();
		int i[] = { 1, 2, 3, 4, 5 };
		int j[] = { 1, 2, 3, 4, 5 };
		System.out.println(a.equal(i, j));
		char k[] = { '哈', '哈', '哈' };
		char L[] = { '哈', '哈', '哈' };
		System.out.println(a.equal(k, L));
	}
}
